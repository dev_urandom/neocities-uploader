# Neocities Uploader

This bash script allows people to upload the contents of a local directory to a
Neocities website, using their API (which means it will work on both free and
supporter accounts).

# How to use

First, you need to create a file called `upload.pwd`, which will have two lines:

```
N_USER=[your neocities username]
N_PWD=[your neocities password]
```

This file will be loaded by the script every time it's executed.

Then, you need to create the `out` directory and put the files containing your
website into that directory.

Whenever the script is executed, it will ask neocities for a list of files that
already exist on the website, compare their hashes to the hashes of the files in
the `out` directory and upload any files that don't exist, or are different, to
the website.

Since cURL seems to have a problem with really long requests, the script only
uploads 20 files at a time.
